module.exports = async function (context, req) {
    context.log('JavaScript HTTP trigger function processed a request.');



    if (req.query.name || (req.body && req.body.name)) {
        let message = "Hello " + (req.query.name || req.body.name);
        context.bindings.QueueMessage = message + " to Queue";
        context.res = {
            status: 200,
            body: message + " to HTTP return"
        };
    }
    else {
        context.res = {
            status: 400,
            body: "Please pass a name on the query string or in the request body"
        };
    }
};